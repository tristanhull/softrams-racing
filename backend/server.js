const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');
const app = express();
const UserModel = require('./auth/model/admin');

mongoose.connect('mongodb://127.0.0.1:27017/softrams-racing');
mongoose.connection.on('error', error => console.log(error));
mongoose.Promise = global.Promise;

require('./auth/prehooks/auth');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const authRoutes = require('./auth/routes/routes');
const authSecureRoutes = require('./auth/routes/secure-routes');
const memberRoutes = require('./members/routes/routes');
const teamRoutes = require('./teams/routes/routes');


//routes
app.use('/auth', authRoutes);
app.use('/admin', passport.authenticate('jwt', { session: false }), authSecureRoutes);
app.use('/member', passport.authenticate('jwt', { session: false }), memberRoutes);
app.use('/team', passport.authenticate('jwt', { session: false }), teamRoutes);

//error handling
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({ error: err });
});

app.listen('8000', () => {
    console.log('Vrrrum Vrrrum! Server starting!');
  });