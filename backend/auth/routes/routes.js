const express = require('express');
const passport = require('passport');
const jwt = require('jsonwebtoken');



const router = express.Router();

//adduser route
router.post('/addadmin', passport.authenticate('adduser', { session : false }) , async (req, res, next) => {
    res.json({
        message: 'Added User Successfully',
        user: req.user
    });
});

//login route
router.post('/login', async (req, res, next) => {
    passport.authenticate('login', async (err, user, info) => {
        try {
            if (err || !user) {
                const error = new Error('Error occurred');
                return next(error);
            }

            req.login(user, { session: false }, async (error) => {
                if(error) {
                    return next(error);
                }

                const body = { _id: user.id, username: user.username };
                //add experation 
                const token = jwt.sign({user: body}, 'top_secret');
                return res.json({ body, token });
            });
        } catch (error) {
            return next(error);
        }
    })(req, res, next);
});

module.exports = router;