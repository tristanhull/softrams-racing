const express = require('express');

const router = express.Router();

//secure admin profile route
router.get('/info', (req, res, next) => {
    res.json({
        message: 'Accessing secure admin info',
        user: req.user,
        //token: req.query.secret_token
    });
});

module.exports = router;