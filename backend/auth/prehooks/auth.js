const passport = require('passport');
const localStrategy = require('passport-local');
const UserModel = require('../model/admin');

const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;

//add user
passport.use('adduser', new localStrategy({
    usernameField: 'username',
    passwordField: 'password'
}, async (username, password, done) => {
    try {
        const user = await UserModel.create({username, password});
        return done(null, user);
    } catch (error) {
        done(error);
    }
}));

//login
passport.use('login', new localStrategy({
    usernameField: 'username',
    passwordField: 'password'
}, async (username, password, done) => {
    try {
        const user = await UserModel.findOne({ username });
        if(!user) {
            return done(null, false, { message: 'User Not Found' });
        }

        const validate = await user.isValidPassword(password);
        if(!validate) {
            return done(null, false, { message: 'Wrong Password' });
        }

        return done(null, user, { message: 'Login Successful' });
    } catch (error) {
        return done(error);
    }
}));

//verify token
passport.use(new JWTstrategy({
    secretOrKey: 'top_secret',
    //fromAuthHeaderAsBearerToken()
    //fromUrlQueryParameter('secret_token')
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
}, async (token, done) => {
    try {
        return done(null, token.user)
    } catch (error) {
        done(error);
    }
}));