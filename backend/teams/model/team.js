const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TeamSchema =  new Schema({
    name: {
        type: String,
        required: true
    },
    members: [{
        type: Schema.Types.ObjectId,
        required: false
    }]
});

const TeamModel = mongoose.model('team', TeamSchema);

module.exports = TeamModel;