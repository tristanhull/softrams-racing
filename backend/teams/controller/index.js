const queries = require('./../query/');
const TeamModel = require('./../model/team');

const createTeam = async(req, res) => {
    let team = new TeamModel({
        name: req.body.name,
        members: []
    });

    try {
        const newTeam = await queries.createTeam(team);
        return res.json({
            success: true,
            team: newTeam,
            message: "Created team: " + newTeam.name
        });
    } catch (error) {
        return res.json({
            success: false,
            message: "Failed to create team."
        });
    }
};

const putTeam = async(req, res) => {
    try {
        const id = req.params.id;
        const team = req.body;
        const updatedTeam = await queries.editTeam(id, team);
        return res.json({
            success: true,
            team: updatedTeam,
            message: "Updated Team."
        });
    } catch (error) {
        return res.json({
            success: false,
            message: "Failed to edit team."
        });
    }
};

const deleteTeam = async(req, res) => {
    try {
        const id = req.params.id;
        const deletedTeam = await queries.deleteTeam(id);
        return res.json({
            success: true,
            team: deletedTeam,
            message: "Deleted team " + deletedTeam.name
        });
    } catch (error) {
        return res.json({
            success: false,
            message: "Failed to delete team."
        });
    }
};

const getTeam = async(req, res) => {
    try {
        const id = req.params.id;
        const team = await queries.getTeam(id);
        return res.json({
            success: true,
            team: team,
            message: "Got team: " + team.name
        })
    } catch (error) {
        return res.json({
            success: false,
            message: "Failed to get Team;"
        });
    }
};

const getTeams = async(req, res) => {
    try {
        const teams = await queries.getTeams();
        return res.json({
            success: true,
            teams: teams,
            message: "Got all teams."
        });
    } catch (error) {
        return res.json({
            success: false,
            message: "Failed to get teams."
        });
    }
};

module.exports = {
    createTeam,
    putTeam,
    deleteTeam,
    getTeam,
    getTeams
}