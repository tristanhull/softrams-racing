const TeamModel = require('./../model/team');
const mongoose = require('mongoose');

const createTeam = async (teamData) => {
    try {
        let newTeam = await TeamModel.create(teamData);
        return newTeam;
    } catch (error) {
        return error;
    }
};

const editTeam = async (id, teamData) => {
    try {
        let updatedTeam = await TeamModel.findByIdAndUpdate(id, teamData);
        return updatedTeam;
    } catch (error) {
        return error;
    }
}

const deleteTeam = async(id) => {
    try {
        let deletedTeam = await TeamModel.findByIdAndDelete(id);
        return deletedTeam;
    } catch (error) {
        return error;
    }
};

const getTeam = async(id) => {
    try {
        let team = await TeamModel.findById(id);
        return team;
    } catch (error) {
        return error;
    }
};

const getTeams = async() => {
    try {
        let teams = await TeamModel.find();
        return teams;
    } catch (error) {
        return error;
    }
};

module.exports = {
    createTeam,
    editTeam,
    deleteTeam,
    getTeam,
    getTeams
}