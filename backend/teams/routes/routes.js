const express = require('express');
const controller = require('../controller');

const router = express.Router();

router.post('/createteam', controller.createTeam);
router.get('/:id', controller.getTeam);
router.get('/', controller.getTeams);
router.put('/:id', controller.putTeam);
router.delete('/:id', controller.deleteTeam);
module.exports = router;