const express = require('express');
const controller = require('../controller');

const router = express.Router();

router.post('/addmember', controller.addMember);
router.get('/:id', controller.getMember);
router.get('/', controller.getMembers);
router.put('/:id', controller.putMember);
router.delete('/:id', controller.deleteMember)
module.exports = router;