const queries = require('./../query');
const MemberModel = require('./../model/member');

const addMember = async (req, res) => {
    let member = new MemberModel({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        jobTitle: req.body.jobTitle,
        status: req.body.status,
        team: req.body.team
    });
    try {
        const newMember = await queries.createMember(member);
        res.json({
            success: true,
            member: newMember,
            message: "Added new member."
        });

    } catch(error) {
        res.json({
            success: false,
            message: "Failed to add member."
        });
        return error;
    }
};

const getMember = async (req, res) => {
    try {
        const id = req.params.id;
        const member = await queries.getMemberById(id);
        return res.json({
            success: true,
            member: member,
            message: "Got " + member.firstName + " " + member.lastName
        });
    } catch(error) {
        return res.json({
            success: false,
            message: "Failed to get member."
        });
    }
};

const putMember = async (req, res) => {
    try{
        const id = req.params.id;
        const member = req.body;
        const updatedMember = await queries.updateMember(id, member);
        return res.json({
            success: true,
            member: member,
            message: "Updated member information."
        });
    } catch(error) {
        res.json({
            success: false,
            message: "Failed to update member"
        });
        return error;
    }
};

const deleteMember =  async (req, res) => {
    try {
        const id = req.params.id;
        const deletedMember = await queries.deleteMember(id);
        return res.json({
            success: true,
            member: deletedMember,
            message: "Deleted " + deletedMember.firstName + " " + deletedMember. lastName
        });
    } catch (error) {
        return res.json({
            success: false,
            message: "Failed to delete member."
        });
    }
};

const getMembers = async(req, res) => {
    try {
         const members = await queries.getMembers();
         return res.json({
             success: true,
             members: members,
             message: "Got all members"
         });
    } catch (error) {
        return res.json({
            success: false,
            message: "Failed to get members."
        });
    }
};

module.exports = {
    addMember,
    getMember,
    putMember,
    deleteMember,
    getMembers
}