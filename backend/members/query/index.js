const MemberModel = require('./../model/member');
const mongoose = require('mongoose');

const createMember = async (memberData) => {
    try {
        const newMember = await MemberModel.create(memberData);
        return newMember;
    } catch(error) {
        return error;
    }
}

const getMemberById = async (id) => {
    try {
        let member = await MemberModel.findById(id);
        return member;
    } catch(error) {
        return error;
    }
};

const updateMember = async (id, updatedMember) => {
    try {
        let member = await MemberModel.findByIdAndUpdate(id, updatedMember);
        return member;
    } catch (error) {
        return error;
    }
};

const deleteMember = async (id) => {
    try {
        let member = await MemberModel.findByIdAndDelete(id);
        return member;
    } catch (error) {
        return error;
    }
};

const getMembers = async () => {
    try {
        let members = await MemberModel.find();
        return members;
    } catch (error) {
        return error;
    }
};

module.exports = {
    createMember,
    getMemberById,
    updateMember,
    deleteMember,
    getMembers
}