import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Team } from '../_models/team';

@Injectable({ providedIn: 'root' })
export class TeamService {
    constructor(private http: HttpClient) {}

    getAllTeams() {
        return this.http.get<any>(`${environment.apiUrl}/team`).pipe(map((res) => {
            if(res.success) {
                return Object.values(res.teams) as Team[];
            } else {
                return [];
            }
        }));
    }

    getTeamById(id: string) {
        return this.http.get<any>(`${environment.apiUrl}/team/${id}`).pipe(map((res) => {
            if(res.success) {
                return res.team as Team;
            } else {
                return null;
            }
        }));
    }

}