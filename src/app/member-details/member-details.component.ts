import { Component, OnInit, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Member } from '../_models/member';
import { Team } from '../_models/team';
import { MemberService } from '../_services/member.service';
import { TeamService } from '../_services/team.service';

@Component({
  selector: 'app-member-details',
  templateUrl: './member-details.component.html',
  styleUrls: ['./member-details.component.css']
})
export class MemberDetailsComponent implements OnInit, OnChanges {
  memberModel: Member;
  memberForm: FormGroup;
  submitted = false;
  loading = false;
  error = '';
  // alertType: String;
  // alertMessage: String;
  teams = [];

  constructor(private fb: FormBuilder, private router: Router, private memberService: MemberService, private teamService: TeamService) {}

  ngOnInit() {
    this.memberForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      jobTitle: ['', Validators.required],
      team: ['', Validators.required],
      status: ['', Validators.required]
    });

    this.teamService.getAllTeams().subscribe((teams) => {
      this.teams = teams;
    });
  }

  get f() {  
    return this.memberForm.controls;
  }

  addMember() {
    this.submitted = true;

    if (this.memberForm.invalid) return;

    this.loading = true;
    let teamId: string = this.teams.find(team => { return team.name === this.f.team.value; })._id;

    this.memberService.addMember(
      this.f.firstName.value,
       this.f.lastName.value, 
       this.f.jobTitle.value, 
       teamId, 
       this.f.status.value).subscribe(data => {
         this.router.navigate(['/members']);
       },
       error => {
        this.error = error;
        this.loading = false;
       })
    
  }

  ngOnChanges() {}


}
