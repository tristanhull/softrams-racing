import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
 // returnUrl: string;
  error = '';

  constructor(
    private fb: FormBuilder, 
    private router: Router, 
    private route: ActivatedRoute, 
    private authService: AuthService
    ) {
      if(this.authService.currentAdminValue) {
        //TODO: Change redirection if user is logged in already
        this.router.navigate['/members'];
      }
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    //this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() {  
    return this.loginForm.controls;
  }

  login() {
    this.submitted = true;

    if(this.loginForm.invalid) return;

    this.loading = true;
    this.authService.login(this.f.username.value, this.f.password.value)
    .pipe(first())
    .subscribe(
      data => {
        //this.router.navigate([this.returnUrl]);
        this.router.navigate(['/members']);
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }

}
