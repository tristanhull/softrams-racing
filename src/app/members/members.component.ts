import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MemberService } from '../_services/member.service';
import { TeamService } from '../_services/team.service';
import { Team } from '../_models/team';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {
  members = [];

  teamTest: Team;

  constructor(private router: Router, private memberService: MemberService, private teamService: TeamService) {}

  ngOnInit() {
    this.memberService.getAllMembers().subscribe((members) => {
      this.members = members;
      this.members.forEach(member => {

        if(member.team) {
          this.teamService.getTeamById(member.team).subscribe((team) => {
            member.team = team.name;
          });
        } else {
          member.team = "No Team Assigned.";
        }
      });
    });
  }

  goToAddMemberForm() {
    this.router.navigate(['/addmember']);

  }

  editMemberByID(id: number) {}

  deleteMemberById(id: number) {}
}
