import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { Admin } from '../_models/admin';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {
  currentAdmin: Admin;
  constructor(private router: Router, private authService: AuthService) {}
  
  ngOnInit() {
    this.authService.currentAdmin.subscribe(admin => this.currentAdmin = admin);
  }


}
