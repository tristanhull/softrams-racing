export class Member {
    _id: any;
    firstName: string;
    lastName: string;
    jobTitle: string;
    team: string;
    status: string;
  }