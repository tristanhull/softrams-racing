import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './_services/auth.service';
import { Admin } from './_models/admin';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'softrams-racing';
  currentAdmin: Admin;

  constructor(private router: Router, private authService: AuthService) {
    this.authService.currentAdmin.subscribe(admin => this.currentAdmin = admin);
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
